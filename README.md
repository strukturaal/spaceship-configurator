# Spaceship configurator

## Install dependencies

```bash
yarn
```

## Run development server

```bash
yarn start
```

## Create production build

```bash
yarn build
```

## Run tests

```bash
yarn test
```
