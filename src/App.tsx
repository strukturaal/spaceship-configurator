import { Box } from "@xstyled/styled-components";
import SelectColor from "./components/Select/Color";
import SelectPower from "./components/Select/Power";
import SelectWarpDrive from "./components/Select/WarpDrive";
import SelectOptionPackage from "./components/Select/OptionPackage";
import Summary from "./components/Summary";
import Title from "./components/UI/Title";
import { ConfigurationBox, ContentGrid, Page } from "./styles/main";

export default function App() {
  return (
    <Page>
      <ConfigurationBox>
        <Title
          text="Spaceship configurator"
          fontSize="large"
          letterSpacing="0.2em"
          textAlign="center"
        />
        <ContentGrid>
          <Box spaceY={{ _: 32, desktop: 48 }}>
            <SelectColor />
            <SelectPower />
            <SelectWarpDrive />
            <SelectOptionPackage />
          </Box>
          <Summary />
        </ContentGrid>
      </ConfigurationBox>
    </Page>
  );
}
