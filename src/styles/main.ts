import styled, { th } from "@xstyled/styled-components";

export const Page = styled.div`
  width: 100%;
  min-height: 100vh;
  padding: 32px 16px;
  background-color: blue;
`;

export const ConfigurationBox = styled.section`
  max-width: 100%;
  margin: 0 auto;
  padding: 40px 16px 32px;
  border: 1px solid ${th.color("light_green")};
  border-radius: 8px;

  @media (min-width: desktop) {
    max-width: 90vw;
    padding: 40px 32px 32px;
  }

  @media (min-width: lg_desktop) {
    max-width: 70vw;
  }

  @media (min-width: xl_desktop) {
    max-width: 60vw;
  }

  @media (min-width: xxl_desktop) {
    max-width: 50vw;
  }
`;

export const ContentGrid = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 45px;

  @media (min-width: desktop) {
    display: grid;
    grid-template-columns: 66% 34%;
  }
`;
