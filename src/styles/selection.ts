import styled from "@xstyled/styled-components";

export const SummaryList = styled.dlBox`
  display: grid;
  grid-template-columns: 90% 10%;
  padding: 16px;
  letter-spacing: 0.2em;

  @media (min-width: desktop) {
    grid-template-columns: 192px 1fr;
  }

  dt,
  dd {
    font-size: small;
  }

  dt {
    color: light_green;

    &:not(:last-of-type) {
      padding-bottom: 14px;
    }
  }

  dd {
    color: white;
  }

  .total {
    font-size: medium;
  }
`;
