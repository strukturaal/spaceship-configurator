import { StrictMode } from "react";
import ReactDOM from "react-dom";
import { ThemeProvider } from "@xstyled/styled-components";
import theme from "./theme";
import "./styles/_reset.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";

ReactDOM.render(
  <StrictMode>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
