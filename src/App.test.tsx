import { render, screen } from "@testing-library/react";
import Summary from "./components/Summary";

test("Renders the correct sum", () => {
  render(<Summary />);

  const summary = screen.getByTestId("total_sum");

  const color = screen.getByTestId("color_price");
  const colorPrice = parseInt(color.innerHTML, 10);

  const power = screen.getByTestId("power_price");
  const powerPrice = parseInt(power.innerHTML, 10);

  const warpDrive = screen.getByTestId("warp_drive_price");
  const warpDrivePrice = parseInt(warpDrive.innerHTML, 10);

  const optionPackage = screen.getByTestId("option_package_price");
  const optionPackagePrice = parseInt(optionPackage.innerHTML, 10);

  const total =
    1000 + colorPrice + powerPrice + warpDrivePrice + optionPackagePrice;

  expect(summary.innerHTML).toContain(total);
});
