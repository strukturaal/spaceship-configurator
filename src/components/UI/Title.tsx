import { Box } from "@xstyled/styled-components";

interface TitleProps {
  text: string;
  [x: string]: any;
}

export default function Title({ text, ...rest }: TitleProps) {
  return (
    <Box color="light_green" fontSize="medium" {...rest}>
      {text}
    </Box>
  );
}
