import { ReactNode, MouseEventHandler } from "react";
import { Box, useColor } from "@xstyled/styled-components";

interface SelectBoxProps {
  onClick: MouseEventHandler<HTMLSpanElement>;
  selected: boolean;
  children: ReactNode;
  [x: string]: any;
}

export default function SelectBox({
  onClick,
  selected,
  children,
  ...rest
}: SelectBoxProps) {
  const lightGreen = useColor("light_green");
  const dimGreen = useColor("dim_green");

  return (
    <Box
      as="span"
      role="button"
      aria-label="Select spaceship part"
      onClick={onClick}
      display="inline-flex"
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
      w={{ _: 200, sm_tablet: "max-content", desktop: 160 }}
      pt={16}
      px={{ _: 32, desktop: 48 }}
      color={selected ? lightGreen : dimGreen}
      hoverColor="lightGreen"
      border={`1px solid ${selected ? lightGreen : dimGreen}`}
      hoverBorder={`1px solid ${lightGreen}`}
      boxShadow={selected ? "0 4px 4px 0 rgba(0, 0, 0, .25)" : "none"}
      borderRadius={8}
      cursor="pointer"
      transition="all 0.2s ease"
      {...rest}
    >
      {children}
    </Box>
  );
}
