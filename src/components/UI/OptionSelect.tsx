import { ReactNode } from "react";
import { Box } from "@xstyled/styled-components";
import Title from "./Title";

interface OptionSelectProps {
  title: string;
  children: ReactNode;
}

export default function OptionSelect({ title, children }: OptionSelectProps) {
  return (
    <Box>
      <Title text={title} textAlign={{ _: "center", sm_tablet: "left" }} />
      <Box
        display="flex"
        flexWrap="wrap"
        justifyContent={{ _: "center", sm_tablet: "flex-start" }}
        rowGap={16}
        columnGap={32}
        pt={12}
      >
        {children}
      </Box>
    </Box>
  );
}
