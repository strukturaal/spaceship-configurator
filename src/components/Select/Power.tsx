import { useAtom } from "jotai";
import { confAtom, configuration } from "../../state";
import { Box } from "@xstyled/styled-components";
import SelectBox from "../UI/SelectBox";
import OptionSelect from "../UI/OptionSelect";

interface PowerProps {
  value: number;
  price: number;
}

const PowerOption = ({ value, price }: PowerProps) => {
  return (
    <Box display="flex" flexDirection="column" alignItems="center" spaceY={8}>
      <Box as="span" fontSize="small">
        {value} MW
      </Box>
      <Box as="span" fontSize="xsmall">
        +{price}€
      </Box>
    </Box>
  );
};

export default function SelectPower() {
  const [state, setState] = useAtom(confAtom);

  return (
    <OptionSelect title="Select power:">
      {configuration.powers.map((power) => {
        return (
          <SelectBox
            key={power.value}
            onClick={() => setState({ ...state, power })}
            selected={power.value === state.power.value}
            pb={8}
          >
            <PowerOption value={power.value} price={power.price} />
          </SelectBox>
        );
      })}
    </OptionSelect>
  );
}
