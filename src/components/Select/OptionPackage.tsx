import { useAtom } from "jotai";
import { confAtom, configuration } from "../../state";
import { Box } from "@xstyled/styled-components";
import SelectBox from "../UI/SelectBox";
import OptionSelect from "../UI/OptionSelect";

export default function SelectOptionPackage() {
  const [state, setState] = useAtom(confAtom);

  return (
    <OptionSelect title="Select option package:">
      {configuration.packages.map((optionPackage) => {
        return (
          <SelectBox
            key={optionPackage.value}
            px={0}
            onClick={() => setState({ ...state, optionPackage })}
            selected={optionPackage.value === state.optionPackage.value}
          >
            <Box
              display="flex"
              flexDirection="column"
              alignItems="center"
              h={63}
              spaceY={9}
            >
              <Box as="span" fontSize="small">
                {optionPackage.value}
              </Box>
              {optionPackage.price > 0 && (
                <Box as="span" fontSize="small">
                  +{optionPackage.price}€
                </Box>
              )}
            </Box>

            <Box
              w="100%"
              h="100%"
              mt={8}
              p="19px 0 19px 16px"
              bg="dark_green"
              borderRadius={8}
            >
              <Box as="ul">
                {optionPackage.options.map((option) => {
                  return (
                    <Box
                      key={option}
                      as="li"
                      listStylePosition="inside"
                      fontSize="xsmall"
                    >
                      {option}
                    </Box>
                  );
                })}
              </Box>
            </Box>
          </SelectBox>
        );
      })}
    </OptionSelect>
  );
}
