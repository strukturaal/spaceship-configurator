import { useAtom } from "jotai";
import { confAtom, configuration } from "../../state";
import { Box } from "@xstyled/styled-components";
import SelectBox from "../UI/SelectBox";
import OptionSelect from "../UI/OptionSelect";

interface ColorProps {
  value: string;
  price: number;
  name: string;
}

const ColorOption = ({ value, price, name }: ColorProps) => {
  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="center"
      letterSpacing="0.1em"
    >
      <Box as="span" w={64} h={32} bg={name} borderRadius={8} />
      <Box as="span" pt={8} fontSize="xsmall">
        +{price}€
      </Box>
      <Box as="span" pt={8} fontSize="small">
        {value}
      </Box>
    </Box>
  );
};

export default function SelectColor() {
  const [state, setState] = useAtom(confAtom);

  return (
    <OptionSelect title="Select color:">
      {configuration.colors.map((color) => {
        return (
          <SelectBox
            key={color.value}
            onClick={() => setState({ ...state, color })}
            selected={color.value === state.color.value}
            pb={20}
          >
            <ColorOption
              value={color.value}
              price={color.price}
              name={color.name}
            />
          </SelectBox>
        );
      })}
    </OptionSelect>
  );
}
