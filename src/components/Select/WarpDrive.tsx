import { useAtom } from "jotai";
import { confAtom, configuration } from "../../state";
import { Box } from "@xstyled/styled-components";
import SelectBox from "../UI/SelectBox";
import OptionSelect from "../UI/OptionSelect";

interface WarpProps {
  drive: string;
  price: number;
}

const WarpOption = ({ drive, price }: WarpProps) => {
  return (
    <Box display="flex" flexDirection="column" alignItems="center" spaceY={8}>
      <Box as="span" fontSize="small">
        {drive}
      </Box>
      <Box as="span" fontSize="xsmall">
        +{price}€
      </Box>
    </Box>
  );
};

export default function SelectWarpDrive() {
  const [state, setState] = useAtom(confAtom);

  return (
    <OptionSelect title="Warp drive:">
      {configuration.warpDrive.map((drive) => {
        return (
          <SelectBox
            key={drive.value}
            onClick={() => setState({ ...state, warpDrive: drive })}
            selected={drive.value === state.warpDrive.value}
            pb={8}
          >
            <WarpOption drive={drive.value} price={drive.price} />
          </SelectBox>
        );
      })}
    </OptionSelect>
  );
}
