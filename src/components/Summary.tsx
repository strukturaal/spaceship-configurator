import { useCallback } from "react";
import { useAtomValue } from "jotai/utils";
import { confAtom } from "../state";
import { Box, useTheme } from "@xstyled/styled-components";
import { SummaryList } from "../styles/selection";

export default function Summary() {
  const theme = useTheme();

  const { color, power, warpDrive, optionPackage } = useAtomValue(confAtom);

  const basePrice = 1000;
  const totalPrice = useCallback(() => {
    return (
      basePrice +
      color.price +
      power.price +
      warpDrive.price +
      optionPackage.price
    );
  }, [color, power, warpDrive, optionPackage]);

  return (
    <Box
      h="max-content"
      mt={32}
      bg="dark_green"
      border={`1px solid ${theme?.colors.light_green}`}
      borderRadius={8}
    >
      <SummaryList>
        <dt>Base price:</dt>
        <dd>{basePrice}€</dd>

        <dt>Color:</dt>
        <dd data-testid="color_price">+{color.price}</dd>

        <dt>Power:</dt>
        <dd data-testid="power_price"> +{power.price}€</dd>

        <dt>Warp drive:</dt>
        <dd data-testid="warp_drive_price">+{warpDrive.price}€</dd>

        <dt>Option package:</dt>
        <dd data-testid="option_package_price">+{optionPackage.price}€</dd>
      </SummaryList>

      <SummaryList
        p="23px 16px"
        borderTop={`1px solid ${theme?.colors.light_green}`}
      >
        <dt className="total">Total:</dt>
        <dd className="total" data-testid="total_sum">
          {totalPrice()}€
        </dd>
      </SummaryList>
    </Box>
  );
}
