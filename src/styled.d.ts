import "styled-components";
import { ITheme } from "@xstyled/styled-components";

interface AppTheme extends ITheme {
  colors: {
    snow: string;
    volcano: string;
    sky: string;
    dim_green: string;
    light_green: string;
    dark_green: string;
    blue: string;
  };
  fontSizes: {
    xsmall: string;
    small: string;
    medium: string;
    large: string;
  };
  screens: {
    _: number;
    mobile: number;
    sm_tablet: number;
    tablet: number;
    desktop: number;
    lg_desktop: number;
    xl_desktop: number;
    xxl_desktop: number;
  };
}

declare module "styled-components" {
  export interface DefaultTheme extends AppTheme {}
}
