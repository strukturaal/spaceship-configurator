const theme = {
  colors: {
    snow: "#FFF",
    volcano: "#FF7A00",
    sky: "#6BE4FF",
    dim_green: "#5A8F4F",
    light_green: "#7BF762",
    dark_green: "#1C3C16",
    blue: "#051544",
  },
  fontSizes: {
    xsmall: "0.75rem", // 12px
    small: "0.875rem", // 14px
    medium: "1.125rem", // 18px
    large: "1.5rem", // 24px
  },
  screens: {
    _: 0,
    mobile: 368,
    sm_tablet: 540,
    tablet: 768,
    desktop: 1024,
    lg_desktop: 1360,
    xl_desktop: 1580,
    xxl_desktop: 1920,
  },
};

export default theme;
