import { atom } from "jotai";

const baseOptions = ["Air conditioning", "Cloth seats", "Fm radio"];

export const configuration = {
  colors: [
    { value: "Snow", price: 0, name: "snow" },
    { value: "Volcano", price: 100, name: "volcano" },
    { value: "Sky", price: 100, name: "sky" },
  ],
  powers: [
    { value: 100, price: 0 },
    { value: 150, price: 200 },
    { value: 200, price: 500 },
  ],
  warpDrive: [
    { value: "NO", price: 0 },
    { value: "YES", price: 1000 },
  ],
  packages: [
    { value: "Basic", price: 0, options: baseOptions },
    {
      value: "Sport",
      price: 100,
      options: [...baseOptions, "Personal tech support"],
    },
    {
      value: "Lux",
      price: 500,
      options: [...baseOptions, "Chrome wheels", "Window tint", "Subwoofer"],
    },
  ],
};

const initialState = {
  color: configuration.colors[0],
  power: configuration.powers[0],
  warpDrive: configuration.warpDrive[0],
  optionPackage: configuration.packages[0],
};

export const confAtom = atom(initialState);
